Let's use this as the "main" repo for now.
By main I mean for tracking the project and road mapping.
We could also put the django site here.

# Overview

StreamerCoin is a surprisingly complex mix of different software.
StreamerCoin is an Ethereum altcoin that's purpose is to allow streamers to implement tokens (backed by a blockchain) for their viewers. 
At first this starts with viewers gaining tokens passively from watching, which can be sent on frivolous things, like 'drinks' or 'gambling'^1.
Our plan is to implement this with a browser extension that can use a Proof of Work to verify a user is watching the stream.
This will most likely be CryptoNight based like Monero.
The XMR gained from that would be passed back to the streamer as additional revenue, after covering hosting/dev costs.

^1 StreamerCoins and Tokens do not hold any monetary value.

## Technical decisions

We chose Ethereum for the ability to make smart contracts. 
This allows us to have an IRC bot that can call functions that interact with the blockchain.
Ethereum also has a fast transactions, which is important with livestreams.

We choose to not use the main Ethereum for cost. 
First it would require viewers to buy ETH to use this system. 
Second even with transactions costing $0.06, that adds up fast with the number of users, as we do most of the transactions.
That left us with using a testnet (we evlauated Rinkeby). 
ETH on testnets don't have a monetary value, so transactions are 'free'. 
We also evaluated the difficutly of running a private net, and we loved the results, so we will continue to use our own network. 
Now this technically means we are on a separate network, we are not ruling out changing now nodes work, so we are considering this a altcoin.
This altcoin will be known as "Streamer Coin (STRM)"

We wanted to make this accessible and neat to viewers. 
This required a good amount of work to cut down the number of steps needed for a user to do a basic command.
This first started with an online wallet, we chose [MyEtherWallet](https://www.myetherwallet.com/). 
This needed to be cut down, warnings removed, and our network and contracts added.
In the future we plan on having a Django service that allows for your wallet to be stored, to make that part easier as well.

# The system

## Contracts
Contracts are currently done by hand in done with [an online sol editor](https://remix.ethereum.org) but will soon migrate to using [truffle](https://github.com/trufflesuite/truffle) to allow for testing. 
They are modified versions of the example tokens provided by [Ethereum.org](https://ethereum.org/token). 
Our contacts are all located here:

[Streamer Coin Contracts](https://gitlab.com/handycodejob/StreamerCoinContracts)

## IRC/Broker
We are using a light IRC bot to bridge irc and the blockchain.
This involves 3 parts:

1. [Twisted](http://twistedmatrix.com) - To keep the broker active and manage an
irc interface.

2. [Web3.py](https://github.com/ethereum/web3.py) - To interface with the
Ethereum blockchain through geth. This allows us to read and write to the
blockchain in pyhton with the contracts we have created.

3. [geth](https://github.com/ethereum/go-ethereum) - To connect to the network
and be a private endpoint for the broker to key its wallet. This will propagate
transactions to the rest of the network.

Some of the base irc bot comes from a [words example](https://github.com/twisted/twisted/blob/twisted-17.9.0/docs/words/examples/ircLogBot.py) in the twisted repo, the rest, modules to interact with the blockchain from irc commands, are original, and located here:

[Streamer Coin IRC](https://gitlab.com/handycodejob/StreamerCoinIRC)


## Wallet
For an easy user experience, we decided to use [MyEtherWallet](https://github.com/kvhnuke/etherwallet).
Our fork of this doesn't have many additions, the major one being json to add our network and contract, taking advantage of their existing system. 
Other than that, we have some divs hidden, which may be completely removed to save bandwidth. 
Our copy of myetherwallet is here:

[Streamer Coin Web Wallet](https://gitlab.com/handycodejob/MyStreamerCoinWallet)


## Blockchain Explorer
While working on the private net, we soon started to miss being able to see transactions and follow addresses for testing. 
Myetherwallet also has framework to link to a blockchain explorer, so we are taking advantage of that as well.
We found [ETHExplorer V2](https://github.com/carsenk/explorer) to fit our needs.
Main changes are moving from bower to yarn. Our fork is here:

[Custom Streamer Coin explorer](https://gitlab.com/handycodejob/explorer)


# How to

The fist step to using StreamerCoin is to create your wallet.
You should use the [wallet we provide](https://wallet.streamerlounge.com/), so go ahead and open that in a tab.
Now to create your wallet, this is the proccess:

 1. You are greated with the create new wallet page conviently, so go ahead and give a password to your wallet.
 2. After you hit "Create New Wallet" you will see a page telling you to "save your Keystore file".
    This is your actual wallet, so download it like it says, but don't worry, it won't be worth only a billion in the future.
 3. Next it will show you your "Private Key".
    Don't worry about saving that, if you remeber your password and don't loose your wallet, you won't need it.
 4. Now it wants you to unlock your wallet.
    To do that, you will select the "Keystore / JSON File", find where you saved your wallet and select it
    Now enter your password to unlock it.
 5. If you scroll down, you will see your wallet.
    On the left you can see your Account Address, mine is `0x6E4b9DC0A0E46236EAE5F483fff772476090D1D0`.
    Yours will be different, but it will always be that long, and start with a `0x`.
    If you look below that, you can see the "Token Balances", if you click "Show All Tokens" you can see how many BLSK you have.
 6. You don't have any BlasKoins you say?!
    Well, first you need to let the Bot know you have a wallet.
    Go into twitch chat and register with StreamerCoinBot, the command I used is this:
    ```
    $register 0x0000000NOT00A00REAL00ADDRESS00000
    ```
    Yours will have your address, not that fake oen of course, copy-paste is your friend here.
    The bot should let you know they have submited it.
 7. Next you will need to give the bot permission to spend your tokens.
    This means if you wanted to give some tokes to your friend, the bot can do it for you!
    On the online wallet, click on the "Contracts" tab.
    Now select the BlasKoin Token Contract from the dropdown on the right.
    It will add in all of the need information so you can interact with the tokens.
 8. On the "Select a function" dropdown, select `approve`
    It will then want you to fill a `_value` field, there you will put how many BlasKoins the bot is allowed to spend.
    What is neat, is you don't need to have the tokens to approve there use.
    Now that doesn't mean the bot will send tokens you don't have, it means when you do get BlasKoins, the bot is already allowed to use them.
    This could be something that you can play around with, but for now `99999` or some other large number will work.
    That will just make sure the bot can spend a good amount without you having to approve more.
 9. Below that it will ask "How would you like to access your wallet?"
    We are going to do the same thing as we did in #4.
    So select your wallet file, and then unlock it with your password.
 10. Now that the wallet is unlock, hit "write".
     It will give you a warning, and you don't have to change anything, just hit "Generate Transaction".
     It will show you what the trasaction looks like, and select the "Yes, I am sure! Make Transaction" button.
     It will let you know it has broadcasted the TX.
 11. Now let's get back to twitch chat and see what we have done!
     Let's use the `$allowance` command.
     The bot will let you know how much it is allowed to spend. 
     Now let's see how many tokens we have, use `$balance`.
     Oh.... 0 BLSK, well hopefully someone will give you some!
     Did someone give you some tokes, check again with `$balance`
 12. Now lets do something with them!
     How about we send some to a friend.
     That command looks like:
     ```
     $pay fsmimp 10
     ```
     Where `fsmimp` is the twitch name of who I want to pay, and `10` is how many tokens to send.

